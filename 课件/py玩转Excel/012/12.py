import pandas as pd
import matplotlib.pyplot as plt

students = pd.read_excel('Students.xlsx', index_col='From')
print(students)
# 数字列,用方括号

# 顺时针
#students['2017'].sort_values(ascending=True).plot.pie(fontsize=8)
#students['2017'].plot.pie(fontsize=8,counterclock=False)
students['2017'].sort_values(ascending=True).plot.pie(fontsize=8,startangle=90)
plt.title('student',fontsize=18,fontweight='bold')
plt.ylabel('2017',fontsize=10,fontweight='bold')
plt.show()
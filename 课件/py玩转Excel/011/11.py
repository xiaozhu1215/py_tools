import pandas as pd
import matplotlib.pyplot as plt

users=pd.read_excel('Users.xlsx')
users['Total']=users['Oct']+users['Nov']+users['Dec']
users.sort_values(by='Total',inplace=True,ascending=False)
print(users.columns)

#users.plot.bar(x='Name',y=['Oct', 'Nov', 'Dec'],stacked=True)
users.plot.barh(x='Name',y=['Oct', 'Nov', 'Dec'],stacked=True)
plt.tight_layout()
plt.title('users')
plt.show()
import pandas as pd
import matplotlib.pyplot as plt

stu=pd.read_excel('Students.xlsx',)
stu.sort_values(by='Number',inplace=True,ascending=False)
stu.plot.bar(x='Field',y='Number',color='red',title='stu by Field')

#显示全部标签
plt.tight_layout()

plt.show()
print(stu)
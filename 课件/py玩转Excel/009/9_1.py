import pandas as pd
import matplotlib.pyplot as plt

stu=pd.read_excel('Students.xlsx',)
stu.sort_values(by='Number',inplace=True,ascending=False)
print(stu.columns)
# stu.plot.bar(x='Field',y='Number',color='red',title='stu by Field')

# 显示全部标签
plt.bar(stu.Field,stu.Number,color='g')

# 旋转90度
plt.xticks(stu.Field,rotation='90')
plt.xlabel('Field')
plt.ylabel('Number')

plt.title('stu by Field',fontsize=26)

plt.tight_layout()
plt.show()

print(stu)
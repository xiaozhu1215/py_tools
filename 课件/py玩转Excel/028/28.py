import pandas as pd
import numpy as np
page_1=pd.read_excel('Students.xlsx',sheet_name='Page_001')
page_2=pd.read_excel('Students.xlsx',sheet_name='Page_002')
# 和append差不多，axis=0:从上到下，axis=1:从左到右
# stu=pd.concat([page_1,page_2]).reset_index(drop=True)
stu=pd.concat([page_1,page_2],axis=0).reset_index(drop=True)
stu['Age']=np.arange(0,len(stu))
# 删除列
stu.drop(columns=['Age','Score'],inplace=True)
# 插入列
stu.insert(1,column='Foo',value=np.repeat('foo',len(stu)))
# 重命名列名
stu.rename(columns={'Foo':'FOO','Name':'NAME'},inplace=True)
# 除掉空值操作
# 1
stu['ID'].loc[5,15]=np.nan
print(stu)
# 2
# stu.ID=stu.ID.astype(float)
# for i in range(5,15):
#     stu['ID'].at[i]=np.nan
# 删除空值的行
stu.dropna(inplace=True)
print(stu)


import pandas as pd

def highest_score_green2(col):
    return ['background-color:lime' if v==col.max() else 'background-color:red' for v in col]

students = pd.read_excel('Students.xlsx')
students.style.apply(highest_score_green2, subset=['Test_1', 'Test_2', 'Test_3'])

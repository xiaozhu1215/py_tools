import pandas as pd
import seaborn as sns

color_map = sns.light_palette('green', as_cmap=True)

students = pd.read_excel('Students.xlsx')
students.style.background_gradient(cmap=color_map, subset=['Test_1','Test_2','Test_3'])

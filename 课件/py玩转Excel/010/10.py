import matplotlib.pyplot as plt
import pandas as pd
stu=pd.read_excel('Students.xlsx')
stu.sort_values(by='2017',inplace=True,ascending=False)
print(stu)
#stu.plot.bar(x='Field',y=['2016','2017'],color=['red','green'],title='stu by field')
stu.plot.bar(x='Field',y=['2016','2017'],color=['red','green'])
plt.title('stu by field',fontsize=36,fontweight='bold')
#fontweight='bold'  粗体

plt.xlabel('Field',fontweight='bold')
plt.ylabel('Number',fontweight='bold')
ax=plt.gca()
ax.set_xticklabels(stu['Field'],rotation=45,ha='right')
f=plt.gcf()
f.subplots_adjust(left=0.2,bottom=0.42)
plt.tight_layout()
plt.savefig('a.png')
plt.show()

import pandas as pd
import matplotlib.pyplot as plt

weeks=pd.read_excel('Orders.xlsx',index_col='Week')
print(weeks)
print(weeks.columns)
# 折线图
# weeks.plot(y=['Accessories', 'Bikes', 'Clothing', 'Components'])

# 叠加区域图
# weeks.plot.area(y=['Accessories', 'Bikes', 'Clothing', 'Components'])

# 叠加柱状图,stacked积累的
weeks.plot.bar(y=['Accessories', 'Bikes', 'Clothing', 'Components'],stacked=True)
plt.title('week',fontsize=16,fontweight='bold')
plt.xticks(weeks.index,fontsize=6,fontweight='bold')
plt.show()
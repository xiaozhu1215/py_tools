import pandas as pd
import matplotlib.pyplot as plt

pd.options.display.max_columns = 999
homes = pd.read_excel('home_data.xlsx')
# print(homes.head())

#scatter 散点图
#homes.plot.scatter(x='sqft_living',y='price')

#bins设置分布区间(桶),hist直方图
# homes.sqft_living.plot.hist(bins=100)
# plt.xticks(range(0,max(homes.sqft_living),500),fontsize=5,rotation=90)

#密度图
# homes.sqft_living.plot.kde()
# plt.xticks(range(0,max(homes.sqft_living),500),fontsize=5,rotation=90)
# plt.show()

# 相关性
print(homes.corr())
import pandas as pd
# 多重排序

product=pd.read_excel('List.xlsx',index_col='ID')
#单列排序
# product.sort_values(by='Price',inplace=True,ascending=False)
# product.sort_values(by='Worthy',inplace=True,ascending=False)

#多列排序
product.sort_values(by=[ 'Worthy','Price'],inplace=True,ascending=[True,False])
print(product.columns)
print(product)


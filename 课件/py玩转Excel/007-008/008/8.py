import pandas as pd
#筛选
def age_18_to_30(a):
    return 18<=a<=30

def level_a(a):
    return 85<=a<=100

stu=pd.read_excel('Students.xlsx',index_col='ID')
# stu=stu.loc[stu.Age.apply(age_18_to_30)].loc[stu['Score'].apply(level_a)]
stu=stu.loc[stu.Age.apply(lambda a:18<=a<=30)] \
    .loc[stu.Score.apply(lambda a:85<=a<=100)]
print(stu)

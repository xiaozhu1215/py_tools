import pandas as pd

def add_2(x):
    return x+2

books=pd.read_excel('Books.xlsx',index_col='ID')
print(books.columns)
# 直接操作一列,操作符重载
# books['Price']=books['ListPrice']*books['Discount']

# for i in books.index:
#     books['Price'].at[i]=books['ListPrice'].at[i]*books['Discount'].at[i]
#左闭右开
# for i in range(5,16):
    # books['Price'].at[i]=books['ListPrice'].at[i]*books['Discount'].at[i]+2
# books['Price']=books['ListPrice'].apply(add_2)
books['Price']=books['ListPrice'].apply(lambda x:x+2)
print(books)


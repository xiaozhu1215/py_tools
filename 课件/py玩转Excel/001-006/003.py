import pandas as pd
#生成序列对象,与字典很像
# s1=pd.Series()
# s1.data
# s1.name
# s1.index

d={'x':1,'y':2,'z':3}
#返回列表
print(d.values())
print(d.keys())

s1=pd.Series(d)
print(s1)
# print(s1.name)
# print(s1.index)
# print(s1.values)

L1=[1,2,3]
L2=['x', 'y', 'z']
s2=pd.Series(L1,index=L2)
s3=pd.Series([1,2,3],index=['x', 'y', 'z'])
print(s2)
print(s3)

k1=pd.Series([1,2,3],index=[1,2,3],name='A')
k2=pd.Series([10,20,30],index=[1,2,3],name='B')
#k3=pd.Series([100,200,300],index=[4,2,3],name='C')
k3=pd.Series([100,200,300],index=[1,2,3],name='C')

df1=pd.DataFrame({k1.name:k1,k2.name:k2,k3.name:k3})


# 以list形似,把每个序列看为一行,把序列的name看为行号
df2=pd.DataFrame([k1,k2,k3])
print(df1)
print(df2)


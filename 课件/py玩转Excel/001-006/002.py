import pandas as pd

people=pd.read_excel('People.xlsx')

# print(people.shape)
# print(people.head())
# print(people.head)
# #列名不消失
# print(people.tail(3))
# print(people.columns)

import pandas as pd
#默认的header=0，隔开几个写几个
# 空行情况下，不用写header=n，自动处理
# index_col='ID' ,读取的时候,自动设置默认列名,要不然会生成0123...
people1=pd.read_excel('People1.xlsx',header=2,index_col='ID')
print(people1)

# 无header的情况
people2=pd.read_excel('People2.xlsx',header=None)
people2.columns=['ID', 'Type', 'Title', 'FirstName', 'MiddleName', 'LastName']
people2.to_excel('People22.xlsx',index=False)
# people2=people2.set_index('ID')

#inplace=True 直接替换,不用赋值.
people2.set_index('ID',inplace=True)
people2.to_excel('People23.xlsx')
print(people2.columns)
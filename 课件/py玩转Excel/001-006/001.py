import pandas as pd

# 创建exel文件,DataFrame相当于sheet
df=pd.DataFrame({'id':[1,2,3],'name':['tim','q','wx']})
print(df)
#df.to_excel('output.xlsx',index=False)
# 设置索引
df=df.set_index('id')

print(df)
df.to_excel('output.xlsx')
import pandas as pd
# 数据校验

def score_valication(row):
    try:
        assert 0 <= row.Score <= 100
    except:
        print(f'#{row.ID}\tstudent {row.Name} has an invalid score {row.Score}')


students = pd.read_excel('Students.xlsx')
# print(students)
students.apply(score_valication, axis=1)

import pandas as pd
def score_val1(row):
# assert（断言）用于判断一个表达式，
# 在表达式条件为false 的时候触发异常。
# >>> assert True     # 条件为 true 正常执行
# >>> assert False    # 条件为 false 触发异常
    try:
        assert 0<=row.Score<=100
    except:
        print(f'#{row.ID}\tstudent {row.Name} has an invalid score {row.Score}')

def score_val2(row):
    if not 0<=row.Score<=100:
        print(f'#{row.ID}\tstudent {row.Name} has an invalid score {row.Score}')


stu=pd.read_excel('Students.xlsx')
stu.apply(score_val2,axis=1)
#axis=0 从上到下
#axis=1 从左到右

print(stu)
import pandas as pd

students1 = pd.read_csv('Students.csv', index_col='ID')
students2 = pd.read_csv('Students.tsv', sep='\t', index_col='ID')
students3 = pd.read_csv('Students.txt', sep='|', index_col='ID')

print(students1)
print(students2)
print(students3)

import pandas as pd
from datetime import date
import matplotlib.pyplot as plt
from scipy.stats import linregress

sales=pd.read_excel('Sales.xlsx',dtype={'Month':str})
print(sales)
slope,intercept,r,p,std_err=linregress(sales.index,sales.Revenue)
# 预测值
exp=sales.index*slope+intercept

# 柱状图
plt.bar(sales.index,sales.Revenue)
plt.plot(sales.index,exp,color='orange')
plt.title(f"y={slope}*x+{intercept}")
# 打印2019.10的预测值
print('2019.10的预测值:  '+str(slope*35+intercept))
plt.xticks(sales.index,sales.Month,rotation=90)
plt.tight_layout()
plt.show()



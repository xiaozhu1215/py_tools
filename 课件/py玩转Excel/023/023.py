import pandas as pd
from datetime import date
import numpy as np
# dtype={'Date': date} 导入时指定数据类型
pd.options.display.max_columns=999
orders = pd.read_excel('Orders.xlsx', dtype={'Date': date})
print(orders.shape)
print(orders.columns)
print(orders)
orders['Year'] = pd.DatetimeIndex(orders.Date).year
groups = orders.groupby(['Category', 'Year'])
s = groups['Total'].sum()
c = groups['ID'].count()
pt1 = pd.DataFrame({'Sum': s, 'Count': c})
pt2 = orders.pivot_table(index='Category', columns='Year', values='Total', aggfunc=np.sum)

print(pt1)
print(pt2)
